# Cours de DEEP LEARNING - M1
## AVEC Pierre Leroy

### Introduction
Ce cours a pour but de découvrir et d'utiliser le deep learning.
Les notes du cours se trouve sur oneNote : https://onedrive.live.com/redir?resid=38E9A41875976226%21686&page=Edit&wd=target%28Deep%20learning.one%7Cae42b124-99e8-475d-a2ee-31b26e16f3fa%2Ftensorflow%7Caea46d10-5d77-49c5-9930-c4e4760bab42%2F%29
Les diapos sur Moodle : https://moodle.ynov.com/course/view.php?id=8150
Slack : https://app.slack.com/client/T010G9CTPST/C010EGD7DA4
Teams pour les cours en direct

### Comment ça marche
#### Prérequis
Docker sur sa machine
#### Lancer le projet
Docker-compose --build
Docker-compose up
